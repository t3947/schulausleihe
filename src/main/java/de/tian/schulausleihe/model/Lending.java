/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Data
@Entity
public class Lending implements IEntity<Long> {

    @Id
    @GeneratedValue(generator = "lending_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(
            name = "lending_sequence",
            sequenceName = "lending_sequence",
            allocationSize=1
    )
    private Long id;
    
    @NotNull
    @Column(nullable = false)
    private LocalDate lendingDate;
    private LocalDate returnDate;
    @ManyToOne(optional = false)
    @NotNull
    private Teacher borrower;
    @ManyToOne
    private MediaItem item;
    private String notes;

}
