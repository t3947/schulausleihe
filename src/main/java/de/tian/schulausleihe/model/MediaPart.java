/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Data
@Entity
public class MediaPart implements IEntity<Long>{
    
    @Id
    @GeneratedValue(generator = "mediapart_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(
            name = "mediapart_sequence",
            sequenceName = "mediapart_sequence",
            allocationSize=1
    )
    private Long id;
    
    @Column(nullable = false)
    private String name;
    private String description;
    
    
    /* Parent sollte nicht in ToString and EqualsAndHashCode berücksichtigt werden,
    *  da es sonst aufgrund rekursiven Aufruf (vgl. to String in Media -> parts)
    *  zu einem Überlauf bzw. einer Endlosschleife kommen wird!
    */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name="parent_id", nullable=false)
    private MediaInformation parent;
    @ManyToOne
    private MediaType type;
    
}
