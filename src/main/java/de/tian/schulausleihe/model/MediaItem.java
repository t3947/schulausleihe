/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Data
@Entity
public class MediaItem {
    
    @Id
    @GeneratedValue(generator = "mediaitem_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(
            name = "mediaitem_sequence",
            sequenceName = "mediaitem_sequence",
            allocationSize=1
    )
    private Long id;
    @Column(nullable=false)
    private LocalDate acquirementDate;
    private LocalDate sortingDate;
    private String notes;
    
    @ManyToOne(optional = false)
    private Budget budget;
    
    @ManyToOne(optional = false)
    private Location location;
    
    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private MediaInformation information;

    public MediaItem() {
    }
    
    /**
     * Copy-Constructor
     * 
     * Copy all properties except the id;
     * 
     * @param other 
     */
    public MediaItem(MediaItem other) {
        this.budget = other.budget;
        this.information = other.information;
        this.location = other.location;
        this.notes = other.notes;
        this.acquirementDate = other.acquirementDate;
        this.sortingDate = other.sortingDate;
        
    }
    
    
    
    
}
