/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.Data;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Data
@Entity
public class MediaInformation {

    @Id
    @GeneratedValue(generator = "media_id_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(
            name = "media_id_seq",
            sequenceName = "media_id_seq",
            allocationSize=1
    )
    private Long id;

    @Column(nullable = false)
    private String name;
    private String description;
    @Column(nullable = false)
    private Integer publicationYear;
    @ManyToOne
    private Publisher publisher;
    @ManyToOne(optional = false)
    private MediaType type;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Keyword> keywords = new HashSet<>();
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Topic> topics = new HashSet<>();
    
    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)//()
    private Set<MediaPart> parts = new HashSet<>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Author> authors = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "information", cascade=CascadeType.ALL)
    private List<MediaItem> stock = new ArrayList<>();
;

}
