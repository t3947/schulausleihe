/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Data
@Entity
public class Teacher implements IEntity<Long>{

    @Id
    @GeneratedValue(generator = "teacher_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(
            name = "teacher_sequence",
            sequenceName = "teacher_sequence",
            allocationSize=1
    )
    private Long id;

    private String firstName;
    private String lastName;
    private boolean active;

    public Teacher(String firstName, String lastName, boolean active) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
    }
    
    public Teacher(String firstName, String lastName) {
            this(firstName, lastName, true);
    }


    public Teacher() {
        this("", "", true);
    }
    
    public String getFullName(){
        StringBuilder sb = new StringBuilder();
        sb.append(lastName);
        sb.append(", ");
        sb.append(firstName);
        return sb.toString();
    }

}
