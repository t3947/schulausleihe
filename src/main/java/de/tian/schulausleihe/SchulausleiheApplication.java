package de.tian.schulausleihe;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan("de.tian.schulausleihe.model")
@EnableJpaRepositories(basePackages = "de.tian.schulausleihe.repo") 
@EnableTransactionManagement
public class SchulausleiheApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchulausleiheApplication.class, args);
	}

}
