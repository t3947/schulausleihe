/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.repo;

import de.tian.schulausleihe.model.Budget;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long>{
    List<Budget> findByName(String name);
}
