/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.repo;

import de.tian.schulausleihe.model.MediaInformation;
import java.time.LocalDate;
import org.springframework.data.jpa.domain.Specification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Repository
public interface MediaInformationRepository extends JpaRepository<MediaInformation, Long>, JpaSpecificationExecutor<MediaInformation>{

    public static Specification<MediaInformation> acquirementDateIsGreater(LocalDate acquirementDate) {
        return (media, cq, cb) -> cb.greaterThanOrEqualTo(media.get("acquirementDate"), acquirementDate);
    }

    public static Specification<MediaInformation> hasType(String typeName) {
        return (media, cq, cb) -> cb.equal(cb.lower(media.get("type").get("name")), typeName.trim().toLowerCase());
    }
/*
    public static Specification<MediaInformation> acquirementDateIsLesser(LocalDate acquirementDate) {
        return (media, cq, cb) -> cb.lessThanOrEqualTo(media.get("acquirementDate"), acquirementDate);
    }*/

    public static Specification<MediaInformation> nameContains(String name) {
        return (media, cq, cb) -> cb.like(media.get("name"), "%" + name + "%");
    }
    
}
