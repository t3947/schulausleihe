/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.repo;

import de.tian.schulausleihe.model.Topic;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Repository
public interface TopicRepository extends JpaRepository<Topic, Long>{
    List<Topic> findByName(String name);
}
