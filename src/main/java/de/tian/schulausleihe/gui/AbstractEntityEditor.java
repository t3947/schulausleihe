/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import de.tian.schulausleihe.model.IEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
public abstract class AbstractEntityEditor<ID, T extends IEntity<ID>> extends VerticalLayout implements KeyNotifier {
    
    protected final JpaRepository<T, ID> repository;

    private T entity;
    
    protected Button save = new Button("Save", VaadinIcon.CHECK.create());
    protected Button cancel = new Button("Cancel");
    protected Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    protected HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    private ChangeHandler changeHandler;
    
    protected void init(){
        
        this.getBinder().bindInstanceFields(this);
        this.add(actions);
    }
    
    public AbstractEntityEditor(JpaRepository<T, ID> repository) {
        this.repository = repository;

        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());

        save.addClickListener(e -> save());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> edit(entity));
        setVisible(false);
    }
    
    public abstract Binder<T> getBinder();

    protected void delete() {
        repository.delete(entity);
        changeHandler.onChange();
    }

    protected void save() {
        repository.save(entity);
        changeHandler.onChange();
    }


    public final void edit(T c) {
        if (c == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = c.getId() != null;
        if (persisted) {
            entity = repository.findById(c.getId()).get();
        } else {
            entity = c;
        }

        cancel.setVisible(persisted);
        this.getBinder().setBean(entity);
        setVisible(true);
        afterEntitySet();
    }
    
    public void afterEntitySet(){};
    
    public void setChangeHandler(ChangeHandler h) {
        changeHandler = h;
    }
    
    
    
    
}
