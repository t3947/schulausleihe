/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import de.tian.schulausleihe.model.MediaInformation;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.data.jpa.domain.Specification;
import de.tian.schulausleihe.repo.MediaInformationRepository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
public class MediaSearchLayout extends VerticalLayout {

    private FormLayout form = new FormLayout();
    private TextField name = new TextField();
    private TextField type = new TextField();
    private TextField topic = new TextField();
    //private  DatePicker acquirementDateFrom = new DatePicker();
    //private  DatePicker acquirementDateTo = new DatePicker();
    //private Checkbox checkboxHideSortedOut = new Checkbox();
    private final MediaInformationRepository repository;

    private final Button searchButton;

    private final List<Consumer<List<MediaInformation>>> resultListener = new ArrayList<>();

    public MediaSearchLayout(MediaInformationRepository repository) {

        this.repository = repository;
        this.form.addFormItem(this.name, "Name");
        this.form.addFormItem(this.type, "Typ");
        this.form.addFormItem(this.topic, "Thema");
        /*
        this.form.addFormItem(this.acquirementDateFrom, "Angeschafft von");
        this.form.addFormItem(this.acquirementDateTo, "Angeschafft bis");
        this.form.addFormItem(this.checkboxHideSortedOut, "Aussortierte ausblenden");
        this.checkboxHideSortedOut.setValue(Boolean.TRUE);
        */        

        this.searchButton = new Button("Suchen", VaadinIcon.SEARCH.create());
        
        this.searchButton.addClickListener(e -> this.handleSearch());
        
        this.add(form);
        this.add(searchButton);

    }

    public void addSearchResultListener(Consumer<List<MediaInformation>> listener) {
        this.resultListener.add(listener);
    }
    
    protected void handleSearch(){
        List<MediaInformation> result = this.search();
        this.resultListener.forEach(l -> l.accept(result));
    }

    protected List<MediaInformation> search (){
        
        Specification<MediaInformation> filter = null;
        List<Specification<MediaInformation>> criterias = new ArrayList<>();
        
        if (!this.name.isEmpty()){
            criterias.add(MediaInformationRepository.nameContains(this.name.getValue()));
        }
        
        if (!this.type.isEmpty()){
            criterias.add(MediaInformationRepository.hasType(this.type.getValue()));
        }  
        /*
        if (!this.topic.isEmpty()){
            criterias.add(MediaInformationRepository.hasTopic(this.topic.getValue()));
        }  */
        
        /*
        if (!this.acquirementDateFrom.isEmpty()){
            criterias.add(MediaInformationRepository.acquirementDateIsGreater(this.acquirementDateFrom.getValue()));
        }
        */
        
        if (!criterias.isEmpty()){
            
            filter = Specification.where(criterias.get(0));
            
            for (int i = 1; i < criterias.size(); i++){
                filter = filter.and(criterias.get(i));
            }
            
            return this.repository.findAll(filter);
        } else {
            return this.repository.findAll();
        }
        
        
        
    }
    
}
