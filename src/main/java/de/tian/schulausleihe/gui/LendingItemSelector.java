/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaItem;
import de.tian.schulausleihe.repo.MediaItemRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LendingItemSelector extends VerticalLayout implements Refreshable{
    
    private final MediaItemRepository repository;
    private final TextField idField = new TextField("ID");
    @Getter
    private final List<Consumer<MediaItem>> resultListeners = new ArrayList<>();
    
    @Autowired
    public LendingItemSelector (MediaItemRepository repository){
        this.repository = repository;
        this.add(idField);
        
    }

    @Override
    public void refresh() {
        this.idField.setValue("");
    }
    
    public MediaItem getItem() {
        if (StringUtils.hasText(idField.getValue())) {
            Long id = Long.parseLong(idField.getValue());
            return repository.findByIdAndSortingDateIsNull(id);
        } 
        return null;
    }
    
    
}
