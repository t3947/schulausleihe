/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.Lending;
import de.tian.schulausleihe.model.MediaItem;
import de.tian.schulausleihe.model.Teacher;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
@Getter
public class LendingCreator extends Dialog {
    
    private final FormLayout formLayout = new FormLayout();

    private Select<Teacher> borrower;
    private DatePicker lendingDate = new DatePicker("Ausleihe am");
    private final TextArea notes = new TextArea("Anmerkungen");
    private final ListCRUDLayoutLendingItems items;

    private Button save = new Button("Anlegen", VaadinIcon.CHECK.create());
    private Button cancel = new Button("Abbrechen");
    private HorizontalLayout actions = new HorizontalLayout(save, cancel);

    private final Binder<Lending> binder = new Binder<>(Lending.class);
    private final List<Consumer<Lending>> saveListeners = new ArrayList<>();

    @Autowired
    public LendingCreator(Select<Teacher> borrower, ListCRUDLayoutLendingItems items) {
        this.borrower = borrower;
        this.binder.bindInstanceFields(this);
        this.items = items;
        this.formLayout.add(this.borrower,
                this.lendingDate,
                this.notes);
        this.add(actions, formLayout, this.items);
        this.cancel.addClickListener(e -> this.close());
        this.save.addClickListener(e -> this.save());

    }

    protected void save() {
        try {
            for (MediaItem item : this.items.getItems()) {
                Lending lending = new Lending();
                this.binder.writeBean(lending);
                lending.setItem(item);
                this.saveListeners.forEach(l -> l.accept(lending));
            }
        } catch (ValidationException ex) {
            Notification.show(ex.getMessage());
        }

    }

}
