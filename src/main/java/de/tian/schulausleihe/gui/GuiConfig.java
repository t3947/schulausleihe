/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import de.tian.schulausleihe.model.Author;
import de.tian.schulausleihe.model.Budget;
import de.tian.schulausleihe.model.Keyword;
import de.tian.schulausleihe.model.Location;
import de.tian.schulausleihe.model.MediaItem;
import de.tian.schulausleihe.model.MediaType;
import de.tian.schulausleihe.model.Publisher;
import de.tian.schulausleihe.model.Teacher;
import de.tian.schulausleihe.model.Topic;
import de.tian.schulausleihe.repo.AuthorRepository;
import de.tian.schulausleihe.repo.BudgetRepository;
import de.tian.schulausleihe.repo.KeywordRepository;
import de.tian.schulausleihe.repo.LocationRepository;
import de.tian.schulausleihe.repo.MediaTypeRepository;
import de.tian.schulausleihe.repo.PublisherRepository;
import de.tian.schulausleihe.repo.TeacherRepository;
import de.tian.schulausleihe.repo.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Configuration
public class GuiConfig {

    @Autowired
    private MediaTypeRepository mediaTypeRepository;

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private KeywordRepository keywordRepository;

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<MediaType> typeSelect() {
        Select<MediaType> result = new Select<>();
        result.setLabel("Typ");
        result.setItemLabelGenerator(MediaType::getName);
        result.setDataProvider(new CallbackDataProvider<MediaType, String>(q -> {
            return mediaTypeRepository.findAll().stream();
        }, q -> {
            return (int) mediaTypeRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Publisher> publisherSelect() {
        Select<Publisher> result = new Select<>();
        result.setLabel("Verlag");
        result.setItemLabelGenerator(Publisher::getName);
        result.setItems(this.publisherRepository.findAll());
        //result.setDataProvider(DataProvider.fromCallbacks(()-> {return this.mediaTypeRepository.findAll()}, return this.mediaTypeRepository.count()};}));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Budget> budgetSelect() {
        Select<Budget> result = new Select<>();
        result.setLabel("Budget");
        result.setItemLabelGenerator(Budget::getName);
        result.setDataProvider(new CallbackDataProvider<Budget, String>(q -> {
            return budgetRepository.findAll().stream();
        }, q -> {
            return (int) budgetRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Teacher> teacherSelect() {
        Select<Teacher> result = new Select<>();
        result.setLabel("Lehrer");
        result.setItemLabelGenerator(Teacher::getFullName);
        result.setDataProvider(new CallbackDataProvider<Teacher, String>(q -> {
            return teacherRepository.findByActiveTrue().stream();
        }, q -> {
            return (int) teacherRepository.findByActiveTrue().size();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Location> locationSelect() {
        Select<Location> result = new Select<>();
        result.setLabel("Standort");
        result.setItemLabelGenerator(Location::getName);
        result.setDataProvider(new CallbackDataProvider<Location, String>(q -> {
            return locationRepository.findAll().stream();
        }, q -> {
            return (int) locationRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Topic> topicSelect() {
        Select<Topic> result = new Select<>();
        result.setItemLabelGenerator(Topic::getName);
        result.setDataProvider(new CallbackDataProvider<Topic, String>(q -> {
            return topicRepository.findAll().stream();
        }, q -> {
            return (int) topicRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Author> authorSelect() {
        Select<Author> result = new Select<>();
        result.setItemLabelGenerator(i -> {
            StringBuilder sb = new StringBuilder();
            sb.append(i.getLastName());
            if (i.getFirstName() != null && !i.getFirstName().isEmpty()) {
                sb.append(", ");
                sb.append(i.getFirstName());
            }
            return sb.toString();
        });
        result.setDataProvider(new CallbackDataProvider<Author, String>(q -> {
            return authorRepository.findAll().stream();
        }, q -> {
            return (int) authorRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Select<Keyword> keywordselect() {
        Select<Keyword> result = new Select<>();
        result.setItemLabelGenerator(Keyword::getName);
        result.setDataProvider(new CallbackDataProvider<Keyword, String>(q -> {
            return keywordRepository.findAll().stream();
        }, q -> {
            return (int) keywordRepository.count();
        }
        ));
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    ListSelectCRUDLayout<Topic> topicListCRUD() {
        ListSelectCRUDLayout<Topic> result = new ListSelectCRUDLayout<>(Topic.class, topicSelect(), "Themen");
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    ListSelectCRUDLayout<Author> authorListCRUD() {
        ListSelectCRUDLayout<Author> result = new ListSelectCRUDLayout<>(Author.class, authorSelect(), "Autoren");
        return result;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    ListSelectCRUDLayout<Keyword> keywordListCRUD() {
        ListSelectCRUDLayout<Keyword> result = new ListSelectCRUDLayout<>(Keyword.class, keywordselect(), "Schlagwörter");
        return result;
    }
    /*
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    ListSelectCRUDLayout<MediaItem> lendingItemListCRUD() {
        ListSelectCRUDLayout<MediaItem> result = new ListSelectCRUDLayout<>(MediaItem.class, keywordselect(), "Schlagwörter");
        return result;
    }
*/
}
