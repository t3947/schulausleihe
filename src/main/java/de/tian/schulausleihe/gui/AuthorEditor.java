/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.tian.schulausleihe.model.Author;
import de.tian.schulausleihe.repo.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@UIScope
public class AuthorEditor extends AbstractEntityEditor<Long, Author>{

    TextField firstName = new TextField("Vorname");
    TextField lastName = new TextField("Nachname");

    private final static Binder<Author> binder = new Binder<>(Author.class);

    @Autowired
    public AuthorEditor(AuthorRepository repository) {
        super(repository);
        
        add(firstName, lastName);
        init();


    }

    @Override
    public Binder<Author> getBinder() {
        return binder;
    }

    @Override
    public void afterEntitySet() {
        this.firstName.focus();
    }
}
