/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.router.Route;
import de.tian.schulausleihe.model.Budget;
import de.tian.schulausleihe.repo.BudgetRepository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Route(value = BudgetView.VIEW_NAME, layout = MainLayout.class)
public class BudgetView extends SimpleItemView<Budget>{
    
    public final static String VIEW_NAME="Budgets";

    public BudgetView(BudgetRepository repository, AbstractEntityEditor<Long, Budget> editor) {
        super(repository, editor, Budget.class);
    }
    
    @Override
    public String getViewName() {
        return VIEW_NAME;
    }

    @Override
    public Budget getNew() {
        return new Budget();
    }
    
}
