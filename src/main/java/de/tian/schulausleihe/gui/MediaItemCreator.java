/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.Budget;
import de.tian.schulausleihe.model.Location;
import de.tian.schulausleihe.model.MediaInformation;
import de.tian.schulausleihe.model.MediaItem;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
@Getter
public class MediaItemCreator extends VerticalLayout {

    protected final FormLayout formLayout = new FormLayout();

    protected final IntegerField amount = new IntegerField("Menge");
    protected final DatePicker acquirementDate = new DatePicker("Anschaffungsdatum");
    protected final Select<Budget> budget;
    protected final Select<Location> location;
    protected final TextArea notes = new TextArea("Anmerkungen");

    protected MediaInformation information;
    protected MediaItem prototype;

    protected Button save = new Button("Anlegen", VaadinIcon.CHECK.create());
    protected Button cancel = new Button("Abbrechen");
    protected HorizontalLayout actions = new HorizontalLayout(save, cancel);

    protected final Binder<MediaItem> binder = new Binder<>(MediaItem.class);
    protected final List<Consumer<MediaItemCreator>> saveListeners = new ArrayList<>();

    @Autowired
    public MediaItemCreator(Select<Budget> budget, Select<Location> location) {
        this.budget = budget;
        this.location = location;
        this.binder.bindInstanceFields(this);
        this.formLayout.add(this.amount, 
                this.acquirementDate, 
                this.budget, this.location, this.notes);
        this.add(actions, formLayout);

        this.save.addClickListener(e -> this.save());

    }

    public MediaInformation getInformation() {
        return information;
    }

    public void setInformation(MediaInformation information) {
        this.information = information;
        this.prototype = new MediaItem();
        this.prototype.setInformation(information);
        this.binder.setBean(prototype);
    }

    protected void save() {
        
        for (int i = 0; i <= this.amount.getValue(); i++) {

            MediaItem copy = new MediaItem(prototype);
            this.information.getStock().add(copy);

        }
        this.saveListeners.forEach(l -> l.accept(this));
    }

}
