/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ListCRUDLayoutMediaItem extends ListCRUDLayout<MediaItem, MediaItemCreator> {

    private final Dialog addDialog = new Dialog();

    @Autowired
    public ListCRUDLayoutMediaItem(MediaItemCreator mediaItemCreator) {
        super(MediaItem.class, "Bestand");
        this.addSupplier = mediaItemCreator;
        addDialog.add(this.addSupplier);
        addDialog.setModal(true);
        this.addButton.addClickListener(e -> addDialog.open());
        this.addSupplier.getCancel().addClickListener(e -> this.addDialog.close());
        this.addSupplier.getSaveListeners().add(s -> this.dataProvider.refreshAll());
        this.addSupplier.getSaveListeners().add(s -> this.addDialog.close());
        
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.removeAllColumns();
        grid.addColumn(MediaItem::getId).setHeader("ID").setWidth("50px").setFlexGrow(0);
        grid.addColumn(item -> item.getLocation().getName()).setHeader("Standort");
        grid.addColumn(item -> item.getBudget().getName()).setHeader("Budget");
        grid.addColumn(MediaItem::getAcquirementDate).setHeader("Anschaffungsdatum");
        grid.addColumn(MediaItem::getSortingDate).setHeader("Aussortiert");
        grid.getColumns().forEach(c -> c.setSortable(true));


    }

}
