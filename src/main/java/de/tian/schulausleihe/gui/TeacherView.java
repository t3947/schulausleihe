/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import de.tian.schulausleihe.model.Teacher;
import de.tian.schulausleihe.repo.TeacherRepository;
import org.springframework.util.StringUtils;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Route(value = TeacherView.VIEW_NAME, layout = MainLayout.class)
public class TeacherView extends VerticalLayout {
    
    public static final String VIEW_NAME = "Teachers";
    private final TeacherRepository teacherRepository;

    private final TeacherEditor editor;

    final Grid<Teacher> grid;

    final TextField filter;

    private final Button addNewBtn;
    
    public TeacherView(TeacherRepository repo, TeacherEditor editor) {
        this.teacherRepository = repo;
        this.editor = editor;
        this.grid = new Grid<>(Teacher.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New teacher", VaadinIcon.PLUS.create());

        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("200px");
        grid.setColumns("id", "firstName", "lastName", "active");
        grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);

        filter.setPlaceholder("Filter by last name");

        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listTeacher(e.getValue()));

        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.edit(e.getValue());
        });

        addNewBtn.addClickListener(e -> editor.edit(new Teacher("", "")));

        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listTeacher(filter.getValue());
        });

        listTeacher(null);
    }

    void listTeacher(String filterText) {
        if (StringUtils.hasText(filterText)) {
            grid.setItems(teacherRepository.findByLastNameStartsWithIgnoreCase(filterText));
        } else {
            grid.setItems(teacherRepository.findAll()); 
        }
    }
}