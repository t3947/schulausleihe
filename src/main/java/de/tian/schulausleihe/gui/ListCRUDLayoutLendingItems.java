/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaItem;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ListCRUDLayoutLendingItems extends ListCRUDLayout<MediaItem, LendingItemSelector> {
    
    @Getter
    private final Set<MediaItem> items = new HashSet<>();
    
    @Autowired
    public ListCRUDLayoutLendingItems(LendingItemSelector lendingItemSelector) {
        super(MediaItem.class, "Ausleihe");

        this.addSupplier = lendingItemSelector;
        this.setDataProvider(new ListDataProvider(items));
        this.addButton.addClickListener(e -> {
            MediaItem item = null;
            item = this.addSupplier.getItem();
            if (item != null) {
                this.items.add(item);
                this.dataProvider.refreshAll();
                this.addSupplier.refresh();
            }
        });
        this.remove(grid);
        this.add(addSupplier);
        this.add(grid);
        
        grid.removeAllColumns();
        grid.addColumn(MediaItem::getId).setHeader("ID");
        grid.addColumn(item -> item.getInformation().getName()).setHeader("Name");
        grid.getColumns().forEach(c -> c.setSortable(true));

    }

}
