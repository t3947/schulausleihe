/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToLongConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.Lending;
import de.tian.schulausleihe.model.Teacher;
import de.tian.schulausleihe.repo.LendingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
public class LendingEditor extends Dialog{
    

    protected LendingRepository lendingRepository;
    private Lending bean;
    
    Binder<Lending> binder = new Binder<>(Lending.class);
    
    
    protected FormLayout layout = new FormLayout();
    protected TextField id = new TextField();
    protected Select<Teacher> borrower;
    protected TextField media = new TextField();
    protected DatePicker lendingDate = new DatePicker();
    protected DatePicker returnDate = new DatePicker();
       

    protected Button save = new Button("Speichern", VaadinIcon.CHECK.create());
    protected Button cancel = new Button("Abbrechen");
    protected HorizontalLayout actions = new HorizontalLayout(save, cancel);
   
    @Autowired
    public LendingEditor(LendingRepository lendingRepository, Select<Teacher> borrower){
        this.lendingRepository = lendingRepository;
        this.borrower = borrower;
        
        layout.addFormItem(lendingDate, "Ausgeliehen");
        layout.addFormItem(returnDate, "Rückgabe");
        layout.addFormItem(media, "Medium");
        layout.addFormItem(borrower, "Ausleiher");

        this.add(layout);
        this.add(actions);
        this.setModal(true);
        this.setCloseOnOutsideClick(false);
        
        save.addClickListener(e -> save());
        cancel.addClickListener(e -> this.close());
        
        
        this.binder.forField(id)
                   .withConverter(new StringToLongConverter("Must enter a number"))
                   .withNullRepresentation(0L)
                   .bind(Lending::getId, Lending::setId);               
        
        this.binder.bindInstanceFields(this);
        
    }
    
    @Override
    public void open(){
        this.open(new Lending());
        
    }
    
    public void open(Lending bean){
        this.setBean(bean);
        super.open();
        
    }
    
    protected Lending getBean(){
        return this.bean;
    }
    
    protected void setBean(Lending bean){
        this.bean = bean;
        this.binder.setBean(this.bean);
    }
    
    @Override
    public void close(){
        this.setBean(null);
        super.close();
    }
    
    protected void save() {
        lendingRepository.save(bean);
        this.close();
    }
   
    
}
