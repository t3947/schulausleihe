/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.tian.schulausleihe.model.Teacher;
import de.tian.schulausleihe.repo.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
    
@SpringComponent
@UIScope
public class TeacherEditor extends AbstractEntityEditor<Long, Teacher> {

    TextField firstName = new TextField("First name");
    TextField lastName = new TextField("Last name");
    Checkbox active = new Checkbox("Active");

    private final static Binder<Teacher> binder = new Binder<>(Teacher.class);

    @Autowired
    public TeacherEditor(TeacherRepository repository) {
        
        super(repository);
        
        add(firstName, lastName, active);
        init();


    }

    @Override
    public Binder<Teacher> getBinder() {
        return binder;
    }

    @Override
    public void afterEntitySet() {
        this.firstName.focus();
    }

}

