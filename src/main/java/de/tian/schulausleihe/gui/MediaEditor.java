/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToLongConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.Author;
import de.tian.schulausleihe.model.Keyword;
import de.tian.schulausleihe.model.MediaInformation;
import de.tian.schulausleihe.model.MediaType;
import de.tian.schulausleihe.model.Publisher;
import de.tian.schulausleihe.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import de.tian.schulausleihe.repo.MediaInformationRepository;
import de.tian.schulausleihe.repo.MediaItemRepository;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
public class MediaEditor extends Dialog implements Refreshable {

    protected final static Logger LOG = LoggerFactory.getLogger(MediaEditor.class);

    private MediaInformationRepository mediaRepository;
    private MediaInformation bean;

    Binder<MediaInformation> binder = new Binder<>(MediaInformation.class);

    protected final VerticalLayout layout = new VerticalLayout();
    protected final VerticalLayout basicDataLayout = new VerticalLayout();
    protected final VerticalLayout extDataLayout = new VerticalLayout();
    protected final VerticalLayout stockLayout = new VerticalLayout();

    private final TextField id = new TextField("ID");
    private final TextField name = new TextField("Name");
    private final IntegerField publicationYear = new IntegerField("Erscheinungsjahr");
    private final ListSelectCRUDLayout<Topic> topics;
    private final ListSelectCRUDLayout<Keyword> keywords;
    private final ListSelectCRUDLayout<Author> authors;
    private final ListCRUDLayoutMediaPart parts;
    private final ListCRUDLayoutMediaItem stock;
    private final MediaItemEditor itemEditor;

    private final Select<MediaType> type;
    private final Select<Publisher> publisher;

    private final TextArea description = new TextArea("Beschreibung");

    private Button save = new Button("Speichern", VaadinIcon.CHECK.create());
    private Button cancel = new Button("Schließen");
    private HorizontalLayout actions = new HorizontalLayout(save, cancel);

    @Autowired
    public MediaEditor(MediaInformationRepository mediaRepository,
            MediaItemRepository stockRepository,
            Select<MediaType> typeSelect,
            Select<Publisher> publisherSelect,
            ListSelectCRUDLayout<Topic> topics,
            ListSelectCRUDLayout<Keyword> keywords,
            ListSelectCRUDLayout<Author> authors,
            ListCRUDLayoutMediaItem stock,
            ListCRUDLayoutMediaPart parts,
            MediaItemEditor itemEditor
    ) {

        this.mediaRepository = mediaRepository;
        this.type = typeSelect;
        this.publisher = publisherSelect;
        this.topics = topics;
        this.authors = authors;
        this.keywords = keywords;
        this.parts = parts;

        this.stock = stock;
        this.itemEditor = itemEditor;
        this.itemEditor.setChangeHandler(stock::refresh);
        this.stock.getGrid().asSingleSelect().addValueChangeListener(e -> {
            this.itemEditor.edit(e.getValue());
        });

        setSizeFull();

        this.id.setEnabled(false);

        Tabs tabs = new Tabs();
        Tab basicDataTab = new Tab("Basisinformationen");

        Tab extDataTab = new Tab("Weitere Informationen");
        Tab stockTab = new Tab("Bestand");

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(basicDataTab, basicDataLayout);
        tabsToPages.put(extDataTab, extDataLayout);
        tabsToPages.put(stockTab, stockLayout);
        tabs.add(basicDataTab, extDataTab, stockTab);

        this.description.setWidthFull();

        this.basicDataLayout.add(id, name, type, publicationYear, publisher, description);
        this.extDataLayout.add(authors, topics, keywords, parts);
        this.extDataLayout.setVisible(false);
        this.stockLayout.add(stock, this.itemEditor);
        this.stockLayout.setVisible(false);

        Div pages = new Div(basicDataLayout, extDataLayout, stockLayout);

        tabs.addSelectedChangeListener(event -> {
            tabsToPages.values().forEach(page -> page.setVisible(false));
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
        });

        this.add(actions);
        this.add(tabs, pages);

        this.setModal(true);
        this.setCloseOnOutsideClick(false);

        save.addClickListener(e -> save());
        cancel.addClickListener(e -> this.close());

        this.binder.forField(id)
                .withConverter(new StringToLongConverter("Must enter a number"))
                .withNullRepresentation(0L)
                .bind(MediaInformation::getId, MediaInformation::setId);

        this.binder.bindInstanceFields(this);

    }

    @Override
    public void open() {
        this.open(new MediaInformation());
    }

    public void open(MediaInformation bean) {
        this.refresh();
        this.setBean(bean);
        super.open();

    }

    protected MediaInformation getBean() {
        return this.bean;
    }

    protected void setBean(MediaInformation bean) {
        this.bean = bean;
        this.stock.getAddSupplier().setInformation(bean);
        this.parts.setMediaInformation(bean);
        this.binder.setBean(this.bean);
        if (this.bean != null) {
            stock.setList(this.bean.getStock());
            parts.setList(this.bean.getParts());
            topics.setList(this.bean.getTopics());
            authors.setList(this.bean.getAuthors());
            keywords.setList(this.bean.getKeywords());
        }

    }

    @Override
    public void close() {
        this.bean = null;
        super.close();
    }

    protected void save() {
        try {
            mediaRepository.save(bean);
            Notification.show("Datensatz gespeichert.");
            this.refresh();
        } catch (DataIntegrityViolationException ex) {
            LOG.error("Fehler:", ex);
            new ErrorMessage(ex).open();

        }
    }

    @Override
    public void refresh() {
        this.type.getDataProvider().refreshAll();
        this.publisher.getDataProvider().refreshAll();
        this.authors.refresh();
        this.topics.refresh();
        this.keywords.refresh();
        this.parts.refresh();
        this.stock.refresh();
        this.binder.setBean(bean);
    }

}
