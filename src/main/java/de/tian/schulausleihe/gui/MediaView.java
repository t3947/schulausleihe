/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.tian.schulausleihe.model.MediaInformation;
import de.tian.schulausleihe.repo.MediaInformationRepository;
import java.util.Arrays;



/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Route(value = MediaView.VIEW_NAME, layout = MainLayout.class)
public class MediaView extends VerticalLayout {

    public final static String VIEW_NAME = "Medien";

    private final MediaInformationRepository repository;
    private final MediaSearchLayout searchCriterias;
    private final MediaEditor editor;

    final Grid<MediaInformation> grid;

    private final Button editBtn;
    
    public MediaView(MediaInformationRepository repository, MediaEditor editor) {
        this.repository = repository;
        this.searchCriterias = new MediaSearchLayout(this.repository);
        this.editor = editor;
        this.grid = new Grid<>();
        this.editBtn = new Button("Edit", VaadinIcon.EDIT.create());
        this.editBtn.setEnabled(false);

        HorizontalLayout actions = new HorizontalLayout(editBtn);
        add(searchCriterias, actions, grid);

        grid.addColumn(MediaInformation::getId).setHeader("ID").setWidth("50px").setFlexGrow(0);
        grid.addColumn(MediaInformation::getName).setHeader("Name");
        grid.addColumn(info -> info.getType().getName()).setHeader("Typ");
        grid.addColumn(info -> {

        return Arrays.toString(info.getTopics().stream().map(t -> t.getName()).toArray());

       }).setHeader("Themen");
            
        
        
        this.searchCriterias.addSearchResultListener(l -> this.grid.setItems(l));
        
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        
        grid.asSingleSelect().addValueChangeListener(e -> {
            editBtn.setEnabled(true);
        });
        editBtn.addClickListener(e -> grid.getSelectionModel().getFirstSelectedItem().ifPresent(editor::open));
        //Funktioniert noch nicht richtigthis.editor.addDialogCloseActionListener(e -> this.grid.sort(null));
    }
    

}
