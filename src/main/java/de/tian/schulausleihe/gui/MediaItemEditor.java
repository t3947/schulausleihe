/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToLongConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.Budget;
import de.tian.schulausleihe.model.Location;
import de.tian.schulausleihe.model.MediaItem;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
public class MediaItemEditor extends VerticalLayout {

    private final FormLayout formLayout = new FormLayout();

    private final TextField id = new TextField("ID");
    private final DatePicker acquirementDate = new DatePicker("Anschaffungsdatum");
    private final DatePicker sortingDate = new DatePicker("Aussortierungsdatum");
    private final Select<Budget> budget;
    private final Select<Location> location;
    private final TextArea notes = new TextArea("Anmerkungen");

    private Optional<ChangeHandler> changeHandler;

    private final Binder<MediaItem> binder = new Binder<>(MediaItem.class);
    private MediaItem bean;

    private Button save = new Button("Speichern", VaadinIcon.CHECK.create());
    private Button cancel = new Button("Abbrechen");
    private HorizontalLayout actions = new HorizontalLayout(save, cancel);

    @Autowired
    public MediaItemEditor(Select<Budget> budget, Select<Location> location) {
        this.budget = budget;
        this.location = location;
        this.id.setEnabled(false);
        this.setVisible(false);
        this.binder.forField(id)
                .withConverter(new StringToLongConverter("Must enter a number"))
                .withNullRepresentation(0L)
                .bind(MediaItem::getId, MediaItem::setId);

        this.binder.bindInstanceFields(this);
        this.formLayout.add(this.id,
                this.acquirementDate, this.sortingDate,
                this.budget, this.location, this.notes);

        this.save.addClickListener(e -> save());
        this.cancel.addClickListener(e -> cancel());
        this.add(this.formLayout, this.actions);

    }

    public void edit(MediaItem bean) {
        this.budget.getDataProvider().refreshAll();
        this.location.getDataProvider().refreshAll();
        this.binder.readBean(bean);
        this.bean = bean;
        this.setVisible(true);

    }

    protected void cancel() {
        this.bean = null;
        this.binder.readBean(bean);
    }

    protected void save() {

        try {
            this.binder.writeBean(bean);
            this.changeHandler.ifPresent(c -> c.onChange());
            this.bean = null;
            this.binder.readBean(bean);
        } catch (ValidationException ex) {
            Notification.show(ex.getMessage());
        }
    }

    public void setChangeHandler(ChangeHandler h) {
        changeHandler = Optional.of(h);
    }

}
