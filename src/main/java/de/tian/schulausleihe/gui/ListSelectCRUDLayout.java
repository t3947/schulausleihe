/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.select.Select;


public class ListSelectCRUDLayout<T> extends ListCRUDLayout<T, Select<T>> {

    public ListSelectCRUDLayout(Class<T> type, Select<T> selectForAdd, String labelText) {
        super(type, labelText);
        this.addSupplier = selectForAdd;
        this.actions.add(selectForAdd);
        this.providers.add(addSupplier.getDataProvider());
        this.addButton.addClickListener(e -> this.addSupplier.getOptionalValue().ifPresent(v -> {
            dataProvider.getItems().add(v);
            dataProvider.refreshAll();
        }));
    }   
    
    
}
