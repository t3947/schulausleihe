/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.tian.schulausleihe.model.IEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
public abstract class AbstractEntityView<ID, T extends IEntity<ID>> extends VerticalLayout {

    private final JpaRepository<T, ID> repository;

    private final AbstractEntityEditor<ID, T> editor;

    final Grid<T> grid;

    private final Button addNewBtn;

    public AbstractEntityView(JpaRepository<T, ID> repository, AbstractEntityEditor<ID, T> editor, Class<T> type) {
        this.repository = repository;
        this.editor = editor;
        this.grid = new Grid<>(type);
        this.addNewBtn = new Button("New", VaadinIcon.PLUS.create());

        HorizontalLayout actions = new HorizontalLayout(addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("200px");
        grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);

        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.edit(e.getValue());
        });

        addNewBtn.addClickListener(e -> editor.edit(this.getNew()));

        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            list();
        });

        list();
    }

    public abstract String getViewName();
    public abstract T getNew();

    void list() {
        grid.setItems(this.repository.findAll());
    }
    
    public Grid<T> getGrid(){
        return this.grid;
    }
}
