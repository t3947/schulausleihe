/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaInformation;
import de.tian.schulausleihe.model.MediaPart;
import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Getter
@Setter
public class ListCRUDLayoutMediaPart extends ListCRUDLayout<MediaPart, MediaPartEditor> {

    private final Dialog dialog = new Dialog();
    private MediaInformation mediaInformation;
    
    @Autowired
    public ListCRUDLayoutMediaPart(MediaPartEditor mediaPartEditor) {
        super(MediaPart.class, "Teile");
        this.addSupplier = mediaPartEditor;
        dialog.add(this.addSupplier);
        dialog.setModal(true);
        this.addButton.addClickListener(e -> {this.addSupplier.setBean(new MediaPart());dialog.open();});
        this.addSupplier.getCancel().addClickListener(e -> this.dialog.close());
        this.addSupplier.getSaveListeners().add(s -> {
            MediaPart part = s.getBean();
            part.setParent(mediaInformation);
            mediaInformation.getParts().add(part);
            this.dataProvider.refreshAll();
            this.dialog.close();
        });
        
        grid.removeAllColumns();
        grid.addColumn(MediaPart::getName).setHeader("Name");
        grid.addColumn(part -> part.getType().getName()).setHeader("Typ");
        grid.addColumn(MediaPart::getDescription).setHeader("Beschreibung");


    }

}
