/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaPart;
import de.tian.schulausleihe.model.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Getter
public class MediaPartEditor extends VerticalLayout{

    TextField name = new TextField("Name");
    TextField description = new TextField("Beschreibung");
    
    private final Select<MediaType> type;

    private final Binder<MediaPart> binder = new Binder<>(MediaPart.class);
    
    protected Button save = new Button("Save", VaadinIcon.CHECK.create());
    protected Button cancel = new Button("Cancel");
    protected HorizontalLayout actions = new HorizontalLayout(save, cancel );
    
    protected final List<Consumer<MediaPartEditor>> saveListeners = new ArrayList<>();
    
    private MediaPart bean;

    @Autowired
    public MediaPartEditor(Select<MediaType> type) {
        
        this.type = type;
        
        add(name, description, type, actions);
        
        this.binder.bindInstanceFields(this);
       
        save.addClickListener(e -> save());

    }

    public void setBean(MediaPart bean) {
        this.bean = bean;
        this.binder.setBean(bean);
    }
    
    public MediaPart getBean() {
        return this.bean;
    }
    
    
    protected void save(){

        this.saveListeners.forEach(l -> l.accept(this));
        
    }


}
