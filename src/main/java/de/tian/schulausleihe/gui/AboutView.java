/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.Version;
import de.tian.schulausleihe.ApplicationInfo;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Route(value = "About", layout = MainLayout.class)
@PageTitle("About")
public class AboutView extends VerticalLayout {

    public static final String VIEW_NAME = "About";
    private final ApplicationInfo appInfo;
    final Grid<SimpleEntry> grid;
    
    @Autowired
    public AboutView(ApplicationInfo appInfo) {

        this.grid = new Grid<>(SimpleEntry.class);
        this.appInfo = appInfo;
        add(grid);

        grid.setHeight("200px");
        grid.setColumns("key", "value");
        //grid.getColumnByKey("key").setWidth("50px").setFlexGrow(0);

        grid.setItems(this.list());

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);

    }

    protected List<SimpleEntry> list() {
        List<SimpleEntry> result = new ArrayList<>();

        result.add(new SimpleEntry("Programmname", "Schulausleihe"));
        result.add(new SimpleEntry("Programmversion", this.appInfo.getVersion()));
        result.add(new SimpleEntry("Entwickler", "tian"));
        result.add(new SimpleEntry("Java-Version", System.getProperty("java.version")));
        result.add(new SimpleEntry("Vaadin-Version", Version.getFullVersion()));
        
        
        return result;

    }

}
