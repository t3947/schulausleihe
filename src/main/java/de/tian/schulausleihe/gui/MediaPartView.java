/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import de.tian.schulausleihe.model.MediaInformation;
import de.tian.schulausleihe.model.MediaPart;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@Scope("prototype")
public class MediaPartView extends VerticalLayout {
    
    private final Grid<MediaPart> grid = new Grid<>();
    private final MediaPartEditor editor;
    private final Button addNewBtn = new Button("Neu", VaadinIcon.PLUS.create());
    private MediaInformation parent;
    
    @Autowired
    public MediaPartView(MediaPartEditor editor) {
        this.editor = editor;
        
        HorizontalLayout actions = new HorizontalLayout(addNewBtn);
        HorizontalLayout data = new HorizontalLayout(grid, editor);
        add(actions, data);

        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.setBean(e.getValue());
        });

        //addNewBtn.addClickListener(e -> {MediaPart p = new MediaPart();  p.setParent(parent) ;editor.edit(p);});
        /*
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            list();
        });*/
                
    }
    
    public void setBean(MediaInformation parent){
        this.parent = parent;
        if (this.parent != null){
            this.grid.setDataProvider(new ListDataProvider<>(parent.getParts()));
        } else {
            this.grid.setDataProvider(new ListDataProvider<>(Collections.EMPTY_SET));
        }
    }

    
}
