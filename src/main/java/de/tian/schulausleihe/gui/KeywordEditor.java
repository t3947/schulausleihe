/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.tian.schulausleihe.model.Keyword;
import de.tian.schulausleihe.repo.KeywordRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@SpringComponent
@UIScope
public class KeywordEditor extends AbstractEntityEditor<Long, Keyword>{

    TextField name = new TextField("Bezeichnung");

    private final static Binder<Keyword> binder = new Binder<>(Keyword.class);

    @Autowired
    public KeywordEditor(KeywordRepository repository) {
        super(repository);
        
        add(name);
        init();


    }

    @Override
    public Binder<Keyword> getBinder() {
        return binder;
    }

    @Override
    public void afterEntitySet() {
        this.name.focus();
    }
}
