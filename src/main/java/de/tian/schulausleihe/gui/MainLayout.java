/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.router.RouterLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */

public class MainLayout extends AppLayout implements RouterLayout {
    
    private final static Logger log = LoggerFactory.getLogger(MainLayout.class);
    
    @Autowired
    private MediaEditor mediaEditor;

    public MainLayout() {

        MenuBar menuBar = new MenuBar();
        
        
       
        MenuItem mediaMenuItem = menuBar.addItem("Medien");
        MenuItem sub = mediaMenuItem.getSubMenu().addItem("Erfassen");
        sub.addClickListener(e -> { this.mediaEditor.open();});
        this.addMenuLink(mediaMenuItem, MediaView.VIEW_NAME, "Suchen");
        this.addMenuLink(mediaMenuItem, LendingView.VIEW_NAME, "Ausleihe");


        MenuItem baseDataMenuItem = menuBar.addItem("Stammdaten");
        this.addMenuLink(baseDataMenuItem, TeacherView.VIEW_NAME, "Lehrer");
        this.addMenuLink(baseDataMenuItem, BudgetView.VIEW_NAME, "Budgets");
        this.addMenuLink(baseDataMenuItem, LocationView.VIEW_NAME, "Standorte");
        this.addMenuLink(baseDataMenuItem, MediaTypeView.VIEW_NAME, "Typen");
        this.addMenuLink(baseDataMenuItem, TopicView.VIEW_NAME, "Themen");
        this.addMenuLink(baseDataMenuItem, KeywordView.VIEW_NAME, "Schlagwörter");
        this.addMenuLink(baseDataMenuItem, AuthorView.VIEW_NAME, "Autoren");
        this.addMenuLink(baseDataMenuItem, PublisherView.VIEW_NAME, "Verlage");
              
        //MenuItem configurationMenuItem = menuBar.addItem("Einstellungen");

        MenuItem infoMenuItem = menuBar.addItem("Info");
        this.addMenuLink(infoMenuItem, AboutView.VIEW_NAME);


        addToNavbar(menuBar);
            
    }
    
    
    private void addMenuLink (MenuItem parent, String viewName, String itemName){
        MenuItem sub = parent.getSubMenu().addItem(itemName);
        sub.addClickListener(e -> {sub.getUI().ifPresent(ui -> ui.navigate(viewName));});
        
    }
    
    private void addMenuLink (MenuItem parent, String viewName){
        addMenuLink(parent, viewName, viewName);
        
    }
    

}
