/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.tian.schulausleihe.model.Lending;
import de.tian.schulausleihe.model.MediaItem;
import de.tian.schulausleihe.repo.LendingRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
@Route(value = LendingView.VIEW_NAME, layout = MainLayout.class)
public class LendingView extends VerticalLayout {

    public final static String VIEW_NAME = "Ausleihe";

    private final LendingRepository repository;
    //private final MediaSearchLayout searchCriterias;
    private final LendingEditor editor;
    private final LendingCreator creator;

    final Grid<Lending> grid;

    private final Button editButton;
    private final Button addButton;

    @Autowired
    public LendingView(LendingRepository repository, LendingEditor editor, LendingCreator creator) {
        this.repository = repository;
        //this.searchCriterias = new MediaSearchLayout(this.repository);
        this.editor = editor;
        this.creator = creator;
        this.grid = new Grid<>();
        this.editButton = new Button("Bearbeiten", VaadinIcon.EDIT.create());
        this.addButton = new Button("Anlegen", VaadinIcon.PLUS.create());
        this.editButton.setEnabled(false);

        HorizontalLayout actions = new HorizontalLayout(addButton, editButton);
        //add(searchCriterias, actions, grid);
        add(actions, grid);

        grid.addColumn(lending -> lending.getBorrower().getFullName()).setHeader("Ausleiher");
        grid.addColumn(Lending::getLendingDate).setHeader("Ausgeliehen");
        grid.addColumn(Lending::getReturnDate).setHeader("Rückgabe");
        grid.addColumn(lending -> {
            StringBuilder sb = new StringBuilder();
            MediaItem item = lending.getItem();
            sb.append(item.getInformation().getName());
            sb.append(" (");
            sb.append(item.getId());
            sb.append(")");
            return sb.toString();
        }
        ).setHeader("Medium");

        //this.searchCriterias.addSearchResultListener(l -> this.grid.setItems(l));
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        grid.asSingleSelect().addValueChangeListener(e -> {
            editButton.setEnabled(true);
        });
        editButton.addClickListener(e -> grid.getSelectionModel().getFirstSelectedItem().ifPresent(editor::open));
        addButton.addClickListener(e -> this.creator.open());
        creator.getSaveListeners().add(this.repository::save);
        list();
    }
    
   void list() {
        grid.setItems(this.repository.findAll());
    }

}
