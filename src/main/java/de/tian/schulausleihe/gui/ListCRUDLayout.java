/*
 * Copyright (C) 2021 tian <tian21@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tian.schulausleihe.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import static de.tian.schulausleihe.util.ObjectUtils.ifNullGetElse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author tian <tian21@mailbox.org>
 */
public abstract class ListCRUDLayout<T, AddSupplier> extends VerticalLayout implements Refreshable {

    protected Label label = new Label();

    protected final Button addButton = new Button("Hinzufügen", VaadinIcon.PLUS.create());
    protected final Button deleteButton = new Button("Entfernen", VaadinIcon.TRASH.create());
    
    @Getter
    protected final Grid<T> grid;
    
    protected final List<DataProvider> providers = new ArrayList<>();

    protected AddSupplier addSupplier;

    protected final HorizontalLayout actions = new HorizontalLayout();

    protected ListDataProvider<T> dataProvider;

    public ListCRUDLayout(Class<T> type, String labelText) {
       
        this.label.setText(labelText);

        this.actions.add(addButton, deleteButton);
        this.grid = new Grid<>(type);
        this.add(label, actions, grid);
        this.setDataProvider(new ListDataProvider<>(Collections.EMPTY_LIST));

        this.deleteButton.setEnabled(false);

        if (this.grid.getColumnByKey("id") != null) {
            this.grid.removeColumnByKey("id");
        }

        this.grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        this.grid.getColumns().forEach(c -> c.setSortable(true));
        this.grid.addItemClickListener(e -> this.deleteButton.setEnabled(true));
        
       
        this.deleteButton.addClickListener(e -> {
            this.grid.getSelectedItems().forEach(dataProvider.getItems()::remove);
            dataProvider.refreshAll();
        });

    }

    public void setLabel(String label) {
        this.label.setText(label);
    }

    protected void setDataProvider(ListDataProvider<T> dataProvider) {
        this.dataProvider = dataProvider;
        this.grid.setDataProvider(dataProvider);
    }

    public void setList(Collection<T> list) {

        setDataProvider(new ListDataProvider<>(ifNullGetElse(list, Collections.EMPTY_LIST)));

    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public AddSupplier getAddSupplier() {
        return addSupplier;
    }

    public void setAddSupplier(AddSupplier addSupplier) {
        this.addSupplier = addSupplier;
    }


    public List<DataProvider> getProviders() {
        return providers;
    }
       
    public void refresh() {
       this.providers.forEach(DataProvider::refreshAll);
     }
    
    

}
